// 在首页发表主题  杠子头 20181026
$action = param(1);

// user_login_check();

if($action == 'create') {

    $tid = param(2);
    $quick = param(3);
    $quotepid = param(4);

    $thread = thread_read($tid);
    empty($thread) AND message(-1, lang('thread_not_exists'));

    $fid = $thread['fid'];

    $forum = forum_read($fid);
    empty($forum) AND message(-1, lang('forum_not_exists'));

    $r = forum_access_user($fid, $gid, 'allowpost');
    if(!$r) {
        message(-1, lang('user_group_insufficient_privilege'));
    }

    ($thread['closed'] && ($gid == 0 || $gid > 5)) AND message(-1, lang('thread_has_already_closed'));

    if($method == 'GET') {

        $header['title'] = lang('post_create');
        $header['mobile_title'] = lang('post_create');
        $header['mobile_link'] = url("thread-$tid");

    } else {

        $message = param('message', '', FALSE);
        empty($message) AND message('message', lang('please_input_message'));

        $doctype = param('doctype', 0);
        xn_strlen($message) > 2028000 AND message('message', lang('message_too_long'));

        $thread['top'] > 0 AND thread_top_cache_delete();

        $quotepid = param('quotepid', 0);
        $quotepost = post__read($quotepid);
        (!$quotepost || $quotepost['tid'] != $tid) AND $quotepid = 0;

        $post = array(
            'tid'=>$tid,
            'uid'=>$uid,
            'create_date'=>$time,
            'userip'=>$longip,
            'isfirst'=>0,
            'doctype'=>$doctype,
            'quotepid'=>$quotepid,
            'message'=>$message,
        );
        $pid = post_create($post, $fid, $gid);
        empty($pid) AND message(-1, lang('create_post_failed'));

        // thread_top_create($fid, $tid);

        $post = post_read($pid);
        $post['floor'] = $thread['posts'] + 2;
        $postlist = array($post);

        $allowpost = forum_access_user($fid, $gid, 'allowpost');
        $allowupdate = forum_access_mod($fid, $gid, 'allowupdate');
        $allowdelete = forum_access_mod($fid, $gid, 'allowdelete');

        // 直接返回帖子的 html
        // return the html string to browser.
        $return_html = param('return_html', 0);
        if($return_html) {
            $filelist = array();
            ob_start();
            include _include(APP_PATH.'view/htm/post_list.inc.htm');
            $s = ob_get_clean();

            message(0, $s);
        } else {
            message(0, lang('create_post_sucessfully'));
        }

    }

} elseif($method == 'POST') {

    $subject = htmlspecialchars(param('subject', '', FALSE));
    $message = param('message', '', FALSE);
    $doctype = param('doctype', 0);

    empty($message) AND message('message', lang('please_input_message'));
    mb_strlen($message, 'UTF-8') > 2048000 AND message('message', lang('message_too_long'));

    $arr = array();
    if($isfirst) {
        $newfid = param('fid');
        $forum = forum_read($newfid);
        empty($forum) AND message('fid', lang('forum_not_exists'));

        if($fid != $newfid) {
            !forum_access_user($fid, $gid, 'allowthread') AND message(-1, lang('user_group_insufficient_privilege'));
            $post['uid'] != $uid AND !forum_access_mod($fid, $gid, 'allowupdate') AND message(-1, lang('user_group_insufficient_privilege'));
            $arr['fid'] = $newfid;
        }
        if($subject != $thread['subject']) {
            mb_strlen($subject, 'UTF-8') > 80 AND message('subject', lang('subject_max_length', array('max'=>80)));
            $arr['subject'] = $subject;
        }
        $arr AND thread_update($tid, $arr) === FALSE AND message(-1, lang('update_thread_failed'));
    }
    $r = post_update($pid, array('doctype'=>$doctype, 'message'=>$message));
    $r === FALSE AND message(-1, lang('update_post_failed'));

    message(0, lang('update_successfully'));
    //message(0, array('pid'=>$pid, 'subject'=>$subject, 'message'=>$message));
}
// 首页发表主题结束  杠子头 20181026