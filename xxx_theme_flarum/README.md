# xxx_theme_flarum
xiuno 仿 flarum 模板

## 演示网站：[http://jpdm.net](http://jpdm.net)

## 安装说明：
把下载后的 xxx_theme_flarum 文件夹放在 xiuno/plugin/ 目录下，后台安装即可。

## 安装本模板必须先安装以下插件插件
1. 帖子收藏 v2.5.21 （haya_favorite）
2. 消息 v1.9 （huux_notice）
3. 精华主题 v3.9 （xn_digest）
4. 我的回帖 / My Reply v1.4 （xn_mypost）
5. 已读未读 v3.2 （xn_read_unread）
6. Xiuno BBS 搜索插件 for 4.0 正式版 v3.4 （xn_search）

## 模板截图：
首页截图
![首页截图](https://github.com/wfsdaj/xxx_theme_flarum/blob/master/screenshot-index.png "首页截图")
帖子内容页截图
![帖子内容页截图](https://github.com/wfsdaj/xxx_theme_flarum/blob/master/screenshot-thread.png "帖子内容页截图")
用户页截图
![用户页截图](https://github.com/wfsdaj/xxx_theme_flarum/blob/master/screenshot-user.png "用户页截图")
