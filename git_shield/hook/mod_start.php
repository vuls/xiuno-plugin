<?php exit;

if($action == 'git_shield') {
	
	$tids = param(2);
	$arr = explode('_', $tids);
	$tidarr = param_force($arr, array(0));
	empty($tidarr) AND message(-1, lang('please_choose_thread'));
	
	$git_shield = param('git_shield');
	
	$threadlist = thread_find_by_tids($tidarr);
	
	$r = thread_update($tidarr, array('git_shield'=>$git_shield));
	$r === FALSE AND message(-1, '屏蔽取消失败');
	
	message(0, lang('set_completely'));
	
}

?>