<?php

/*
	Xiuno BBS 4.0 大白_笔记模板
*/

!defined('DEBUG') AND exit('Access Denied.'); 
$setting = setting_get('huux_style_one');

$action = param(3);



if($method == 'GET') {
		 $input = array(); 
		 $input['hso_logo'] = form_textarea('hso_logo', $setting['hso_logo'], '100%', 100);
		 $input['hso_forum_info'] = form_radio_yes_no('hso_forum_info', $setting['hso_forum_info']);

		 $input['hso_background'] = form_text('hso_background', $setting['hso_background']);
		 $input['hso_background_color'] = form_text('hso_background_color', $setting['hso_background_color']);
		 $input['hso_background_repeat'] = form_radio('hso_background_repeat', array('repeat'=>'平铺', 'repeat-x'=>'横向平铺', 'repeat-y'=>'纵向平铺', 'no-repeat'=>'不平铺'), $setting['hso_background_repeat']);
		 $input['hso_background_position'] = form_select('hso_background_position', array('50% 50%'=>'居中', '50% 0%'=>'中上', '50% 100%'=>'中下', '0% 0%'=>'左上', '0% 50%'=>'左中', '0% 100%'=>'左下', '100% 0%'=>'右上', '100% 50%'=>'右中', '100% 100%'=>'右下'), $setting['hso_background_position']);
		 $input['hso_background_attachment'] = form_radio('hso_background_attachment', array('fixed'=>'固定', 'scroll'=>'随页面滚动'), $setting['hso_background_attachment']);

		 $input['hso_site_brief'] = form_radio_yes_no('hso_site_brief', $setting['hso_site_brief']);
		 $input['hso_site_count'] = form_radio('hso_site_count', array(1=>'用户/主题/在线', 2=>'用户/主题/帖子',3=>'全部', 0=>'否',), $setting['hso_site_count']);
		 $input['hso_site_user'] = form_radio_yes_no('hso_site_user', $setting['hso_site_user']);

		 $input['hso_thread_userthread'] = form_radio_yes_no('hso_thread_userthread', $setting['hso_thread_userthread']);
		 $input['hso_thread_menu'] = form_radio('hso_thread_menu', array(1=>'目录+主题数', 2=>'目录' , 0=>'否'), $setting['hso_thread_menu']);
		 $input['hso_index_menu'] = form_radio('hso_index_menu', array(1=>'目录+主题数', 2=>'目录' , 0=>'否'), $setting['hso_index_menu']);
		 $input['hso_title_length'] = form_text('hso_title_length', $setting['hso_title_length'], '100%', 100);

		 $input['hso_thread_back'] = form_radio('hso_thread_back', array(1=>'首页+主题页', 2=>'主题页', 0=>'否'), $setting['hso_thread_back']);
		 $input['hso_thread_cardfooter'] = form_select('hso_thread_cardfooter', array(1=>'分享+上下主题', 2=>'分享+上下主题(有标题)', 3=>'分享', 4=>'上下主题',5=>'上下主题(有标题)', 0=>'否'), $setting['hso_thread_cardfooter']);

		 $input['hso_threadlist1_avatar'] = form_radio('hso_threadlist1_avatar', array(1=>'圆形', 2=>'方形' , 0=>'否'), $setting['hso_threadlist1_avatar']);
		 $input['hso_threadlist1_imgs'] = form_radio('hso_threadlist1_imgs', array(1=>'1张', 2=>'2张', 3=>'3张', 4=>'4张', 5=>'5张', 0=>'否'), $setting['hso_threadlist1_imgs']);

		  $input['hso_thread_user'] = form_radio_yes_no('hso_thread_user', $setting['hso_thread_user']);
		  $input['hso_site_headernav'] = form_select('hso_site_headernav', array(1=>'首页(PC端) | 首页(手机端)+导航', 2=>'首页(PC端) | 首页(手机端)', 3=>'首页(PC端)+导航 | 首页(手机端)+导航',4=>'首页(PC端)+导航 | 首页(手机端)', 5=>'无(PC端) | 首页(手机端)+导航', 6=>'无(PC端) | 首页(手机端)'),  $setting['hso_site_headernav']);
		  $input['hso_col_index'] = form_select('hso_col_index', array(1=>'2:10 (左侧边栏)', 2=>'3:9  (左宽侧边栏)',3=>'10:2 (右侧边栏)', 4=>'9:3  (右宽侧边栏)', 0=>'12(无侧边栏)'), $setting['hso_col_index']);
		  $input['hso_forum1_pagination_header'] = form_radio_yes_no('hso_forum1_pagination_header', $setting['hso_forum1_pagination_header']);

		 include _include(APP_PATH.'plugin/huux_style_one/setting.htm');
					
} else{
	    $setting['hso_logo'] = param('hso_logo', '', FALSE); 
	    $setting['hso_forum_info'] = param('hso_forum_info', 0); 

	    $setting['hso_background'] = param('hso_background', '', FALSE); 
	    $setting['hso_background_color'] = param('hso_background_color', '', FALSE); 
	    $setting['hso_background_repeat'] = param('hso_background_repeat', '', FALSE); 
	    $setting['hso_background_position'] = param('hso_background_position', '', FALSE); 
	    $setting['hso_background_attachment'] = param('hso_background_attachment', '', FALSE);
	    
	    $setting['hso_site_brief'] = param('hso_site_brief', 0);
	    $setting['hso_site_count'] = param('hso_site_count', 0);
	    $setting['hso_site_user'] = param('hso_site_user', 0);

	    $setting['hso_thread_userthread'] = param('hso_thread_userthread', 0);
	    $setting['hso_thread_menu'] = param('hso_thread_menu', 0);  
	    $setting['hso_index_menu'] = param('hso_index_menu', 0);
	    $setting['hso_title_length'] = param('hso_title_length', 0);

	    $setting['hso_thread_back'] = param('hso_thread_back', 0);
	    $setting['hso_thread_cardfooter'] = param('hso_thread_cardfooter', 0);
	    $setting['hso_threadlist1_avatar'] = param('hso_threadlist1_avatar', 0);
	    $setting['hso_threadlist1_imgs'] = param('hso_threadlist1_imgs', 0);

	    

	    $setting['hso_thread_user'] = param('hso_thread_user', 0);
	    $setting['hso_site_headernav'] = param('hso_site_headernav', 0);
	    $setting['hso_col_index'] = param('hso_col_index', 0);
	    $setting['hso_forum1_pagination_header'] = param('hso_forum1_pagination_header', 0);
	    
	    

	    
	    
	    setting_set('huux_style_one', $setting);
		message(0, array('a'=>'修改成功','b'=>'大白友情提示：修改成功','c'=>'失败'));

}

?>