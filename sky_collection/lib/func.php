<?php
/**
 *
 * @author tiankong <tianakong@aliyun.com>
 * @version 1.0
 */

function url_list($config, $num = '')
{
    $url = array();
    switch ($config['sourcetype']) {
        case 'urlpage1'://序列化
            $num = empty($num) ? $config['pagesize_end'] : $num;
            if ($num < $config['pagesize_start']) $num = $config['pagesize_start'];
            for ($i = $config['pagesize_start']; $i <= $num; $i = $i + $config['par_num']) {
                $url[$i] = str_replace('(*)', $i, $config['urlpage']);
            }
            break;
        case 'urlpage2'://多网址
            $url = explode(chr(10), $config['urlpage']);
            break;
        case 'urlpage3'://单一网址

    }
    return $url;
}

function get_url_lists($url, &$config)
{
    if ($html = get_html($url, $config)) {
        $html = cut_html($html, $config['url_start'], $config['url_end']);
        $html = str_replace(array("\r", "\n"), '', $html);
        $html = str_replace(array("</a>", "</A>"), "</a>\n", $html);
        preg_match_all('/<a ([^>]*)>([^\/a>].*)<\/a>/i', $html, $out);
        $data = array();
        foreach ($out[1] as $k => $v) {
            if (preg_match('/href=[\'"]?([^\'" ]*)[\'"]?/i', $v, $match_out)) {
                if ($config['url_contain']) {
                    if (strpos($match_out[1], $config['url_contain']) === false) {
                        continue;
                    }
                }
                if ($config['url_except']) {
                    if (strpos($match_out[1], $config['url_except']) !== false) {
                        continue;
                    }
                }
                $url2 = $match_out[1];
                $url2 = url_check($url2, $url, $config);
                $data[$k]['url'] = $url2;
                $data[$k]['title'] = strip_tags($out[2][$k]);
            } else {
                continue;
            }
        }
        return $data;
    } else {
        return array();
    }
}

function get_html($url, $config)
{
    $opt = array('http' => array('header' => "Referer: " . $config['page_base'], 'timeout' => 30));
    $context = stream_context_create($opt);
    if (!empty($url) && $html = @file_get_contents($url, false, $context)) {
        $html = iconv($config['sourcecharset'], 'utf-8//TRANSLIT//IGNORE', $html);
        return $html;
    } else {
        return false;
    }
}

function cut_html($html, $start, $end)
{
    if (empty($html)) return false;
    $html = str_replace(array("\r", "\n"), "", $html);
    $start = str_replace(array("\r", "\n"), "", $start);
    $end = str_replace(array("\r", "\n"), "", $end);
    $html = explode(trim($start), $html);
    if (is_array($html) && isset($html[1])) $html = explode(trim($end), $html[1]);
    return trim($html[0]);
}

function url_check($url, $baseurl, $config)
{
    $urlinfo = parse_url($baseurl);

    $baseurl = $urlinfo['scheme'] . '://' . $urlinfo['host'] . (substr($urlinfo['path'], -1, 1) === '/' ? substr($urlinfo['path'], 0, -1) : str_replace('\\', '/', dirname($urlinfo['path']))) . '/';
    if (strpos($url, '://') === false) {
        if ($url[0] == '/') {
            //京东、太平洋变态的都是 //www.jd.com/111.html
            if($url[1] == '/') {
                $url = $urlinfo['scheme'] . ':'.$url;
            } else {
                $url = $urlinfo['scheme'] . '://' . $urlinfo['host'] . $url;
            }
        } else {
            if ($config['page_base']) {
                $url = $config['page_base'] . $url;
            } else {
                $url = $baseurl . $url;
            }
        }
    }
    return $url;
}

function new_addslashes($string)
{
    if (!is_array($string)) return addslashes($string);
    foreach ($string as $key => $val) $string[$key] = new_addslashes($val);
    return $string;
}

function get_content($url, $config, $page = 0)
{
    set_time_limit(300);
    static $oldurl = array();
    $page = intval($page) ? intval($page) : 0;
    if ($html = get_html($url, $config)) {
        if (empty($page)) {
            if ($config['title_rule']) {
                $title_rule = replace_sg($config['title_rule']);
                $data['title'] = replace_item(cut_html($html, $title_rule[0], $title_rule[1]), $config['title_html_rule']);
            }
        }
        if ($config['content_rule']) {
            $content_rule = replace_sg($config['content_rule']);
            $data['content'] = replace_item(cut_html($html, $content_rule[0], $content_rule[1]), $config['content_html_rule']);
        }
        if (in_array($page, array(0, 2)) && !empty($config['content_page_start']) && !empty($config['content_page_end'])) {
            $oldurl[] = $url;
            $tmp[] = $data['content'];
            $page_html = cut_html($html, $config['content_page_start'], $config['content_page_end']);
            if ($config['content_page_rule'] == 2 && in_array($page, array(0, 2)) && $page_html) {
                preg_match_all('/<a [^>]*href=[\'"]?([^>\'" ]*)[\'"]?[^>]*>([^<\/]*)<\/a>/i', $page_html, $out);
                if (!empty($out[1]) && !empty($out[2])) {
                    foreach ($out[2] as $k => $v) {
                        if (strpos($v, $config['content_nextpage']) === false) continue;
                        if ($out[1][$k] == '#') continue;
                        $out[1][$k] = url_check($out[1][$k], $url, $config);
                        if (in_array($out[1][$k], $oldurl)) continue;
                        $oldurl[] = $out[1][$k];
                        $results = get_content($out[1][$k], $config, 2);
                        if (!in_array($results['content'], $tmp)) $tmp[] = $results['content'];
                    }
                }
            }
            if ($config['content_page_rule'] == 1 && $page == 0 && $page_html) {
                preg_match_all('/<a [^>]*href=[\'"]?([^>\'" ]*)[\'"]?/i', $page_html, $out);
                if (isset($out[1]) && is_array($out[1]) && !empty($out[1])) {
                    $out = array_unique($out[1]);
                    foreach ($out as $k => $v) {
                        if (isset($out[1][$k]) && $out[1][$k] == '#') continue;
                        $v = url_check($v, $url, $config);
                        $results = get_content($v, $config, 1);
                        if (!in_array($results['content'], $tmp) && $results['content'] != "") $tmp[] = $results['content'];
                    }
                }
            }
            $data['content'] = $config['content_page'] == 1 ? implode('[page]', $tmp) : implode('', $tmp);
        }
        if ($page == 0) {
            $GLOBALS['url'] = $url;
            $GLOBALS['config'] = $config;
            $data['content'] = preg_replace_callback('/<img[^>]*src=[\'"]?([^>\'"\s]*)[\'"]?[^>]*>/i', 'download_img_callback', $data['content']);
            if (empty($page) && !empty($data['content']) && $config['down_attachment'] == 1) {
                $data['content'] = download_file($data['content'], $config['watermark']);
            }
        }
        return $data;
    }
    return null;
}


function replace_sg($html)
{
    $list = explode('[内容]', $html);
    if (is_array($list)) foreach ($list as $k => $v) {
        $list[$k] = str_replace(array("\r", "\n"), '', trim($v));
    }
    return $list;
}

function replace_item($html, $config)
{
    if (empty($config)) return $html;
    $config = explode("\n", $config);
    $patterns = $replace = array();
    $p = 0;
    foreach ($config as $k => $v) {
        if (empty($v)) continue;
        $c = explode('[sky]', $v);
        if (isset($c[0]) && isset($c[1])) {
            $patterns[$k] = '/' . str_replace('/', '\/', $c[0]) . '/i';
            $replace[$k] = $c[1];
        }
        $p = 1;
    }
    return $p ? @preg_replace($patterns, $replace, $html) : false;
}

function download_img_callback($matches)
{
    return download_img($matches[0], $matches[1]);
}

function download_img($old, $out)
{
    if (!empty($old) && !empty($out) && strpos($out, '://') === false) {
        return str_replace($out, url_check($out, $GLOBALS['url'], $GLOBALS['config']), $old);
    } else {
        return $old;
    }
}

function download_file($string, $watermark = '', $ext = 'gif|jpg|jpeg|bmp|png')
{
    global $uid, $conf;
    $uploaddir = $conf['upload_path'] . 'attach/' . date('Ym') . '/';
    $uploadurl = $conf['upload_url'] . 'attach/' . date('Ym') . '/';
    !is_dir($uploaddir) AND mkdir($uploaddir, 0777, TRUE);
    //考虑图片地址不在src属性 todo
    $img_src = 'href|src';
    if (!preg_match_all("/($img_src)=([\"|']?)([^ \"'>]+\.($ext))\\2/i", $string, $matches)) return $string;
    foreach ($matches[3] as $matche) {
        if (strpos($matche, '://') === false) continue;
        $remotefileurls[$matche] = fillurl($matche);
    }
    unset($matches);
    $remotefileurls = array_unique($remotefileurls);
    $oldpath = $newpath = array();
    foreach ($remotefileurls as $k => $file) {
        if (strpos($file, '://') === false) continue;
        $filename = fileext($file);
        if (!preg_match("/($ext)/is", $filename) || in_array($filename, array('php', 'phtml', 'php3', 'php4', 'jsp', 'dll', 'asp', 'cer', 'asa', 'shtml', 'shtm', 'aspx', 'asax', 'cgi', 'fcgi', 'pl'))) {
            continue;
        }
        $file_name = basename($file);
        $filename = $uid . '_' . xn_rand(15) . '.' . $filename;

        $newfile = $uploaddir . $filename;
        if (saveImg($file, $newfile)) {
            $oldpath[] = $k;
            $newpath[] = $uploadurl . $filename;
            @chmod($newfile, 0777);
            if ($watermark > 0) {
                //添加水印
                watermark($newfile, $newfile);
            }
            //关联到附件
            if (isset($GLOBALS['collection_content'])) {
                list($att_width, $attr_height) = getimagesize($newfile);
                $filetypes = include APP_PATH . 'conf/attach.conf.php';
                $attr_type = attach_type($newfile, $filetypes);
                $attr_filename = date('Ym') . '/' . $filename;
                $attr_arr = array(
                    'content_id' => $GLOBALS['collection_content']['id'],
                    'filesize' => filesize($newfile),
                    'width' => $att_width,
                    'height' => $attr_height,
                    'filename' => $attr_filename,
                    'orgfilename' => $file_name,
                    'filetype' => $attr_type,
                    'create_date' => time(),
                    'isimage' => 1
                );
                collection_attach__create($attr_arr);
                //$aid = attach_create($attr_arr);
            }
        }
    }
    return str_replace($oldpath, $newpath, $string);
}

function fillurl($surl, $absurl = '', $basehref = '')
{
    if ($basehref != '') {
        $preurl = strtolower(substr($surl, 0, 6));
        if ($preurl == 'http://' || $preurl == 'ftp://' || $preurl == 'mms://' || $preurl == 'rtsp://' || $preurl == 'thunde' || $preurl == 'emule://' || $preurl == 'ed2k://')
            return $surl;
        else
            return $basehref . '/' . $surl;
    }
    $i = 0;
    $dstr = '';
    $pstr = '';
    $okurl = '';
    $pathStep = 0;
    $surl = trim($surl);
    if ($surl == '') return '';
    $urls = @parse_url($GLOBALS['config']['page_base']);
    $HomeUrl = $urls['host'];
    $BaseUrlPath = $HomeUrl . $urls['path'];
    $BaseUrlPath = preg_replace("/\/([^\/]*)\.(.*)$/", '/', $BaseUrlPath);
    $BaseUrlPath = preg_replace("/\/$/", '', $BaseUrlPath);
    $pos = strpos($surl, '#');
    if ($pos > 0) $surl = substr($surl, 0, $pos);
    if ($surl[0] == '/') {
        $okurl = 'http://' . $HomeUrl . '/' . $surl;
    } elseif ($surl[0] == '.') {
        if (strlen($surl) <= 2) return '';
        elseif ($surl[0] == '/') {
            $okurl = 'http://' . $BaseUrlPath . '/' . substr($surl, 2, strlen($surl) - 2);
        } else {
            $urls = explode('/', $surl);
            foreach ($urls as $u) {
                if ($u == "..") $pathStep++;
                else if ($i < count($urls) - 1) $dstr .= $urls[$i] . '/';
                else $dstr .= $urls[$i];
                $i++;
            }
            $urls = explode('/', $BaseUrlPath);
            if (count($urls) <= $pathStep)
                return '';
            else {
                $pstr = 'http://';
                for ($i = 0; $i < count($urls) - $pathStep; $i++) {
                    $pstr .= $urls[$i] . '/';
                }
                $okurl = $pstr . $dstr;
            }
        }
    } else {
        $preurl = strtolower(substr($surl, 0, 6));
        if (strlen($surl) < 7)
            $okurl = 'http://' . $BaseUrlPath . '/' . $surl;
        elseif ($preurl == "http:/" || $preurl == 'ftp://' || $preurl == 'mms://' || $preurl == "rtsp://" || $preurl == 'thunde' || $preurl == 'emule:' || $preurl == 'ed2k:/')
            $okurl = $surl;
        else {
            //$okurl = 'http://' . $BaseUrlPath . '/' . $surl;
            $okurl = $surl;
        }
    }
    $preurl = strtolower(substr($okurl, 0, 6));
    if ($preurl == 'https:' || $preurl == 'ftp://' || $preurl == 'mms://' || $preurl == 'rtsp://' || $preurl == 'thunde' || $preurl == 'emule:' || $preurl == 'ed2k:/') {
        return $okurl;
    } else {
        $okurl = preg_replace('/^(http:\/\/)/i', '', $okurl);
        $okurl = preg_replace('/\/{1,}/i', '/', $okurl);
        return 'http://' . $okurl;
    }
}

function fileext($filename)
{
    return strtolower(trim(substr(strrchr($filename, '.'), 1, 10)));
}

function saveImg($file, $newfile)
{
    set_time_limit(0);
    $opt = array('http' => array('header' => "Referer: " . $GLOBALS['config']['page_base'], 'timeout' => 50));
    $context = stream_context_create($opt);
    $html = @file_get_contents($file, false, $context);
    if ($html) {
        file_put_contents($newfile, $html);
    }
    return true;
}

function sky_get_url()
{
    $sys_protocal = isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443' ? 'https://' : 'http://';
    $php_self = $_SERVER['PHP_SELF'] ? ($_SERVER['PHP_SELF']) : ($_SERVER['SCRIPT_NAME']);
    $path_info = isset($_SERVER['PATH_INFO']) ? ($_SERVER['PATH_INFO']) : '';
    $relate_url = isset($_SERVER['REQUEST_URI']) ? ($_SERVER['REQUEST_URI']) : $php_self . (isset($_SERVER['QUERY_STRING']) ? '?' . safe_replace($_SERVER['QUERY_STRING']) : $path_info);
    return $sys_protocal . (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '') . $relate_url;
}

function sky_import_data($nodeid, $forum_ids, $ids = array())
{
    global $uid, $longip;

    $nodes = collection_node__read($nodeid);
    if ($ids) {
        foreach ($ids as $key=>$id) {
            $key = $key+1;
            $data = collection_content__read($id);
            if ($data['status'] == 2 || $data['status'] == 0) {
                continue;
            }
            if ($nodes['post_uids']) {
                $uids = explode(',', $nodes['post_uids']);
                $uid_key = array_rand($uids);
                $uid = $uids[$uid_key];
            }
            $contents = json_decode($data['data'], true);
            foreach ($forum_ids as $fid) {
                $pid = 0;
                if ($nodes['post_times']) {
                    $times = explode(',', $nodes['post_times']);
                    $time_key = array_rand($times);
                    $post_time_min = $times[$time_key];
                    $post_time_min = $post_time_min*$key;
                    $time = ($post_time_min * 60) + time();
                } else {
                    $time = time();
                }
                $arr = array();
                $arr['fid'] = $fid; //版块id
                $arr['uid'] = $uid;
                $arr['subject'] = trim($contents['title']);
                $arr['message'] = trim($contents['content']);
                $arr['time'] = $time;
                $arr['longip'] = $longip;
                $arr['doctype'] = 0;
                $tid = thread_create($arr, $pid);
                //关联附件
                $collection_attachs = collection_attach__find(array('content_id'=>$id), array('aid'=>1), 1, 1000);
                if($collection_attachs) {
                    $attac = array();
                    foreach ($collection_attachs as $attac) {
                        unset($attac['aid']);
                        unset($attac['content_id']);
                        $attac['tid'] = $tid;
                        $attac['pid'] = $pid;
                        $attac['uid'] = $uid;
                        attach__create($attac);
                    }
                }
                list($attachlist, $imagelist, $filelist) = attach_find_by_pid($pid);
                $images = count($imagelist);
                $files = count($filelist);
                thread__update($tid, array('images' => $images, 'files' => $files));
                post__update($pid, array('images' => $images, 'files' => $files));
            }
            collection_content__update($id, array('status' => 2));
        }
    } else {
        $datas = collection_content__find(array('status'=>1), array('id'=>-1), 1, 99999999);
        foreach ($datas as $key=>$val) {
            $key = $key+1;
            if ($nodes['post_uids']) {
                $uids = explode(',', $nodes['post_uids']);
                $uid_key = array_rand($uids);
                $uid = $uids[$uid_key];
            }
            $contents = json_decode($val['data'], true);
            foreach ($forum_ids as $fid) {
                $pid = 0;
                if ($nodes['post_times']) {
                    $times = explode(',', $nodes['post_times']);
                    $time_key = array_rand($times);
                    $post_time_min = $times[$time_key];
                    $post_time_min = $post_time_min*$key;
                    $time = ($post_time_min * 60) + time();
                } else {
                    $time = time();
                }
                $arr = array();
                $arr['fid'] = $fid; //版块id
                $arr['uid'] = $uid;
                $arr['subject'] = trim($contents['title']);
                $arr['message'] = trim($contents['content']);
                $arr['time'] = $time;
                $arr['longip'] = $longip;
                $arr['doctype'] = 0;
                $tid = thread_create($arr, $pid);
                //关联附件
                $collection_attachs = collection_attach__find(array('content_id'=>$val['id']), array('aid'=>1), 1, 1000);
                if($collection_attachs) {
                    $attac = array();
                    foreach ($collection_attachs as $attac) {
                        unset($attac['aid']);
                        unset($attac['content_id']);
                        $attac['tid'] = $tid;
                        $attac['pid'] = $pid;
                        $attac['uid'] = $uid;
                        attach__create($attac);
                    }
                }
                list($attachlist, $imagelist, $filelist) = attach_find_by_pid($pid);
                $images = count($imagelist);
                $files = count($filelist);
                thread__update($tid, array('images' => $images, 'files' => $files));
                post__update($pid, array('images' => $images, 'files' => $files));
            }
            collection_content__update($val['id'], array('status' => 2));
        }
    }
}

function watermark($source, $target = '', $w_pos = '', $w_img = '', $w_text = '天空', $w_font = 8, $w_color = '#ff0000')
{
    if (!sky_check_img($source)) return false;
    if (!$target) $target = $source;
    $config = $GLOBALS['config'];
    $w_pos = $config['watermark'];
    if(!file_exists(APP_PATH . $config['watermark_img'])) {
        $w_text = $config['watermark_img'];
        //$w_text = iconv('utf-8', 'GB2312//TRANSLIT//IGNORE', $w_text);
        //$w_text = iconv('GB2312', 'utf-8//TRANSLIT//IGNORE', $w_text);
    } else {
        $w_img = APP_PATH . $config['watermark_img'];
    }
    $source_info = getimagesize($source);
    $source_w = $source_info[0];
    $source_h = $source_info[1];
    if ($source_w < $config['watermark_img_w'] || $source_h < $config['watermark_img_h']) return false;
    switch ($source_info[2]) {
        case 1 :
            $source_img = imagecreatefromgif($source);
            break;
        case 2 :
            $source_img = imagecreatefromjpeg($source);
            break;
        case 3 :
            $source_img = imagecreatefrompng($source);
            break;
        default :
            return false;
    }
    if (!empty($w_img) && file_exists($w_img)) {
        $ifwaterimage = 1;
        $water_info = getimagesize($w_img);
        $width = $water_info[0];
        $height = $water_info[1];
        switch ($water_info[2]) {
            case 1 :
                $water_img = imagecreatefromgif($w_img);
                break;
            case 2 :
                $water_img = imagecreatefromjpeg($w_img);
                break;
            case 3 :
                $water_img = imagecreatefrompng($w_img);
                break;
            default :
                return;
        }
    } else {
        $ifwaterimage = 0;
        $temp = imagettfbbox(ceil($w_font * 2.5), 0, APP_PATH . 'plugin/sky_collection/lib/font/elephant.ttf', $w_text);
        $width = $temp[2] - $temp[6];
        $height = $temp[3] - $temp[7];
        unset($temp);
    }
    switch ($w_pos) {
        case 1:
            $wx = 5;
            $wy = 5;
            break;
        case 2:
            $wx = ($source_w - $width) / 2;
            $wy = 0;
            break;
        case 3:
            $wx = $source_w - $width;
            $wy = 0;
            break;
        case 4:
            $wx = 0;
            $wy = ($source_h - $height) / 2;
            break;
        case 5:
            $wx = ($source_w - $width) / 2;
            $wy = ($source_h - $height) / 2;
            break;
        case 6:
            $wx = $source_w - $width;
            $wy = ($source_h - $height) / 2;
            break;
        case 7:
            $wx = 0;
            $wy = $source_h - $height;
            break;
        case 8:
            $wx = ($source_w - $width) / 2;
            $wy = $source_h - $height;
            break;
        case 9:
            $wx = $source_w - $width;
            $wy = $source_h - $height;
            break;
        case 10:
            $wx = rand(0, ($source_w - $width));
            $wy = rand(0, ($source_h - $height));
            break;
        default:
            $wx = rand(0, ($source_w - $width));
            $wy = rand(0, ($source_h - $height));
            break;
    }
    if ($ifwaterimage) {
        if ($water_info[2] == 3) {
            imagecopy($source_img, $water_img, $wx, $wy, 0, 0, $width, $height);
        } else {
            imagecopymerge($source_img, $water_img, $wx, $wy, 0, 0, $width, $height, 85);
        }
    } else {
        if (!empty($w_color) && (strlen($w_color) == 7)) {
            $r = hexdec(substr($w_color, 1, 2));
            $g = hexdec(substr($w_color, 3, 2));
            $b = hexdec(substr($w_color, 5));
        } else {
            return;
        }
        imagestring($source_img, $w_font, $wx, $wy, $w_text, imagecolorallocate($source_img, $r, $g, $b));
    }

    switch ($source_info[2]) {
        case 1 :
            imagegif($source_img, $target);
            break;
        case 2 :
            imagejpeg($source_img, $target, 80);
            break;
        case 3 :
            imagepng($source_img, $target);
            break;
        default :
            return;
    }

    if (isset($water_info)) {
        unset($water_info);
    }
    if (isset($water_img)) {
        imagedestroy($water_img);
    }
    unset($source_info);
    imagedestroy($source_img);
    return true;
}

function sky_check_img($image)
{
    return extension_loaded('gd') && preg_match("/\.(jpg|jpeg|gif|png)/i", $image, $m) && file_exists($image) && function_exists('imagecreatefrom' . ($m[1] == 'jpg' ? 'jpeg' : $m[1]));
}


?>