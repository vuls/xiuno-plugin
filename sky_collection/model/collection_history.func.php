<?php
/**
 *
 * @author tiankong <tianakong@aliyun.com>
 * @version 1.0
 */

// ------------> 最原生的 CURD，无关联其他数据。

function collection_history__create($arr) {
    $r = db_create('collection_history', $arr);
    return $r;
}

function collection_history__read($md5) {
    $data = db_find_one('collection_history', array('md5url'=>$md5));
    return $data;
}

function collection_history__delete($md5) {
    $r = db_delete('collection_history', array('md5url'=>$md5));
    return $r;
}

function collection_history__find($cond = array(), $orderby = array(), $page = 1, $pagesize = 20) {
    $lists = db_find('collection_history', $cond, $orderby, $page, $pagesize);
    return $lists;
}


function collection_history_count($cond = array()) {
    $n = db_count('collection_history', $cond);
    return $n;
}

?>