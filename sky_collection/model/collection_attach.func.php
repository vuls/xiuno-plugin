<?php
/**
 *
 * @author tiankong <tianakong@aliyun.com>
 * @version 1.0
 */

// ------------> 最原生的 CURD，无关联其他数据。

function collection_attach__create($arr) {
    $r = db_create('collection_attach', $arr);
    return $r;
}

function collection_attach__update($id, $arr) {
    $r = db_update('collection_attach', array('aid'=>$id), $arr);
    return $r;
}

function collection_attach__read($id) {
    $data = db_find_one('collection_attach', array('aid'=>$id));
    return $data;
}

function collection_attach__delete($id) {
    $r = db_delete('collection_attach', array('aid'=>$id));
    return $r;
}

function collection_attach__find($cond = array(), $orderby = array(), $page = 1, $pagesize = 20) {
    $lists = db_find('collection_attach', $cond, $orderby, $page, $pagesize);
    return $lists;
}

function collection_attach_count($cond = array()) {
    $n = db_count('collection_attach', $cond);
    return $n;
}

function collection_attach__delete_cond($cond) {
    $r = db_delete('collection_attach', $cond);
    return $r;
}


function collection_attach__read_cond($cond) {
    $data = db_find_one('collection_attach', $cond);
    return $data;
}

?>