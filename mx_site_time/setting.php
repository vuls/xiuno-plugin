<?php
	!defined('DEBUG') AND exit('Access Denied.');

	$action = param(3);

	if (empty($action)) {
		$site_time_conf = kv_cache_get('site_time');

		if ($method == 'GET') {
			$input['on_or_off'] = form_radio('on_or_off', array(), $site_time_conf['on_or_off'] ? $site_time_conf['on_or_off'] : '1');
			$input['display_words'] = form_text('display_words', $site_time_conf['display_words'] ? $site_time_conf['display_words'] : '');
			$input['current_date'] = form_text('current_date', $site_time_conf['current_date'] ? $site_time_conf['current_date'] : '');

			include _include(APP_PATH.'plugin/mx_site_time/setting.htm');

		} else {
			$site_time_conf['on_or_off'] = param('on_or_off');
			$site_time_conf['display_words'] = param('display_words');
			$site_time_conf['current_date'] = param('current_date');
			$site_time_conf['site_time'] = diffBetweenTwoDays(date("Y-m-d"),$site_time_conf['current_date']);

			empty($site_time_conf['display_words']) AND message('display_words', '请输入要展示的文字');
			empty($site_time_conf['current_date']) AND message('current_date', '请输入网站起始时间');

			/**
			 * 日期规则
			 */
			$date_rules = '/^\d{4}[\/\-](0?[1-9]|1[012])([\/\-](0?[1-9]|[12][0-9]|3[01]))?$/';

			$site_time_conf['current_date'] = trim($site_time_conf['current_date']);

			if (!preg_match($date_rules, $site_time_conf['current_date'])) {
				message('current_date', '日期格式填写错误，格式必须为年-月-日，且年月日必须符合正常日期要求');
			}

			/**
			 * 所填写日期校验
			 */
			strtotime($site_time_conf['current_date']) > strtotime(date("Y-m-d")) AND message('current_date', '所填时间不能大于当前时间');

			kv_cache_set('site_time', $site_time_conf);

			message(0, lang('create_successfully'));
		}
	}




?>
